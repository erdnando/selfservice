import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/providers/local/local_auth.dart';
import 'package:getx_pattern/app/data/providers/remote/autentication_api.dart';
import 'package:getx_pattern/app/data/providers/remote/selfservice_api.dart';
import 'package:getx_pattern/app/data/repositories/local/local_authentication_repository.dart';
import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';
import 'package:getx_pattern/app/data/repositories/remote/selfservice_repository.dart';
import 'package:getx_pattern/app/utils/diobase.dart';

//Carga las dependecias en memoria y las disponibiliza alos widgets en el arbol
//Aqui se puede implementar las unit test, creando mocks
class DependencyInjection {
  static void init() {
    //----------------------------------------------------------------------------------------------
    //Sistema
    //Se cargan clases de sistema para q esten disponibles por los componentes
    Get.lazyPut<DioBase>(
        () => DioBase(baseUrl: "https://selfservice-backend.herokuapp.com"));

    Get.lazyPut<Dio>(
        () => Dio(BaseOptions(baseUrl: 'https://api.themoviedb.org/3')),
        fenix: true);

    //Permite acceso al localstorage
    Get.put<FlutterSecureStorage>(FlutterSecureStorage());
    //----------------------------------------------------------------------------------------------
    //Providers
    Get.put<AuthenticationApi>(AuthenticationApi());

    Get.put<LocalAuth>(LocalAuth());

    Get.put<SelfServiceApi>(SelfServiceApi());
    //----------------------------------------------------------------------------------------------
    //Repositories
    Get.put<AuthenticationRepository>(AuthenticationRepository());

    Get.put<LocalAuthRepository>(LocalAuthRepository());

    Get.put<SelfServiceRepository>(SelfServiceRepository());

    //----------------------------------------------------------------------------------------------
  }
}
