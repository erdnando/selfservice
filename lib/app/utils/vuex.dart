import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';

class Vuex {
//--------------------------------------------------------------------------------------------------------------------------------
  //Variables globales
  static String TOKEN_GENERADO = '';
  static String THE_MOVIE_DB_API_KEY = "0bd141a296b8758dbf4de1e8c0b0b469";
  static String myToken = '';
  static String identifier = 'system@selfservice.com';
  static String password = 'Welcome\$1';
  static String contentType = 'application/json; charset=utf-8';
  static bool loading = true;

//--------------------------------------------------------------------------------------------------------------------------------
  //Funciones y metodos genericos
//--------------------------------------------------------------------------------------------------------------------------------
  //Alert generico
  static dialogo(
      {required String titulo, required String mensaje, Function? accion}) {
    Get.dialog(AlertDialog(
      title: Text(titulo),
      content: Text(mensaje),
      actions: [
        TextButton(
            onPressed: () {
              Get.back();
              accion!();
            },
            style: TextButton.styleFrom(
                backgroundColor: Colors.teal,
                primary: Colors.white,
                onSurface: Colors.grey),
            child: Text("Ok"))
      ],
    ));
  }

  static sinAcceso() {
    goToPage(destino: AppRoutes.SINACCESO);
  }

  //GoTo generico sin poder regresar
  static goToPage({required String destino, dynamic? args}) {
    Get.offNamed(destino,
        arguments:
            args); //si entra en el onready del controller d ela vista inciada
  }

  /* static goToPageRef(Widget paginaDestino) {
    Get.to(paginaDestino, transition: Transition.fade);
  }

  static goToPageRemoveHistory(String paginaDestino) {
    Get.offAllNamed(paginaDestino);
  }*/
}

//--------------------------------------------------------------------------------------------------------------------------------
