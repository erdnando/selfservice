import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/login/login_controller.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) {
        return Scaffold(
          body: GestureDetector(
            //oculta el teclado al click fuera de los campos. Requisito tener color asignado al container
            onTap: () => FocusScope.of(context).unfocus(),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.transparent,
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextField(
                    onChanged: _.onUsernameChanged,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: "Username"),
                  ),
                  TextField(
                    onChanged: _.onPasswordChanged,
                    obscureText: true,
                    decoration: InputDecoration(labelText: "Password"),
                  ),
                  TextButton(
                      onPressed: _.submit,
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Send"))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
