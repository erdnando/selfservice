import 'package:dio/dio.dart';
//import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/models/request_token.dart';
import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class LoginController extends GetxController {
  //inyeccion de dependencias
  final AuthenticationRepository _repository =
      Get.find<AuthenticationRepository>();

  String _username = '', _password = '';

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    //logica inicial
    try {
      print("on controller login...");
      Vuex.dialogo(titulo: "AVISO", mensaje: "login controller al inicio");
    } catch (e) {
      print(e);
    }
  }

  printMessage() {
    print("login controller al click");
  }

  void onUsernameChanged(String text) {
    _username = text;
  }

  void onPasswordChanged(String text) {
    _password = text;
  }

  Future<void> submit() async {
    try {
      RequestToken reqToken = await _repository.newRequestToken();
      Vuex.TOKEN_GENERADO = reqToken.requestToken;

      final RequestToken requestToken = await _repository.validateWithLogin(
          username: _username,
          password: _password,
          requestToken: Vuex.TOKEN_GENERADO);

      print('Login success...${requestToken.expiresAt}');
      Vuex.dialogo(titulo: "SUCCESS", mensaje: "Login successfull!!!!");
    } catch (e) {
      print(e);
      String? message = "";
      if (e is DioError) {
        if (e.response != null) {
          if (e.response!.statusMessage != null) {
            message = e.response!.statusMessage;
          } else {
            message = "";
          }
        } else {
          message = e.message;
        }
      }

      Vuex.dialogo(titulo: "ERROR", mensaje: message!);
    }
  }
}
