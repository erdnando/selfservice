import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/agentes_disponibles/agentes_disponibles_controller.dart';

class AgentesDisponiblesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AgentesDisponiblesController());
  }
}
