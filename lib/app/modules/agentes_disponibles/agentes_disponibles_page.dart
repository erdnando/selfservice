import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/agentes_disponibles/agentes_disponibles_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class AgentesDisponiblesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AgentesDisponiblesController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Agentes disponibles"),
                  SizedBox(height: 150),
                  Text("Agente 1"),
                  Text("Agente 2"),
                  Text("Agente 3"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        Vuex.goToPage(destino: AppRoutes.PERFILES);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Ver perfiles...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
