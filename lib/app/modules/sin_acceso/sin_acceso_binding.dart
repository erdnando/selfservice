import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/sin_acceso/sin_acceso_controller.dart';

class SinAccesoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SinAccesoController());
  }
}
