import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/home/home_controller.dart';
import 'package:getx_pattern/app/modules/home/local_widgets/lstCategorias.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () => print('hi on menu icon'),
                );
              },
            ),
            title: Text('Home'),
            actions: <Widget>[
              IconButton(
                icon: new Icon(Icons.merge_type),
                onPressed: () => print('hi on icon action'),
              ),
            ],
          ),
          body: Container(
              width: double.infinity,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                SizedBox(height: 20),
                Text("Home   ::: Selfservice"),
                SizedBox(height: 30),
                Text("Categorias"),
                SizedBox(height: 10),
                LstCategorias(),
                SizedBox(height: 50),
                TextButton(
                    onPressed: () {
                      Vuex.goToPage(destino: AppRoutes.DETALLECATEGORIA);
                    },
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.teal,
                        primary: Colors.white,
                        onSurface: Colors.grey),
                    child: Text("A detalle categorias...")),
                SizedBox(height: 100),
              ])),
        );
      },
    );
  }
}
