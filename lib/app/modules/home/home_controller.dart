import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/models/categoria.dart';
import 'package:getx_pattern/app/data/repositories/remote/selfservice_repository.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class HomeController extends GetxController {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  final SelfServiceRepository _baseRepository =
      Get.find<SelfServiceRepository>();

  List<Categorias> arrCategorias = [];

  bool _loading = true;
  bool get loading => _loading;

  @override
  void onReady() {
    super.onReady();
    Vuex.loading = true;
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      arrCategorias = await _baseRepository.categorias();
      this._loading = false;
      update(["lstCategorias"]);
    } on DioError catch (ex) {
      this._loading = false;
      if (ex.type == DioErrorType.response) {
        Vuex.dialogo(
            titulo: "ERROR",
            mensaje: "Problemas para obtener categorias: " + ex.message,
            accion: Vuex.sinAcceso());
      }
    }
  }
}
