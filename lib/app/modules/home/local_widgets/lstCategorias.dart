import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/models/categoria.dart';
import 'package:getx_pattern/app/modules/home/home_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class LstCategorias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      id: "lstCategorias",
      builder: (_) {
        if (_.loading) {
          return Container(
              width: double.infinity,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                SizedBox(height: 100),
                CircularProgressIndicator(),
                SizedBox(height: 100),
              ]));
        } else {
          return Expanded(
            child: ListView.builder(
                itemBuilder: (ctx, index) {
                  Categorias c = _.arrCategorias[index];
                  return ListTile(
                    title: new Text(c.categoria),
                    subtitle: new Text('UUID: ${c.categoriaId}'),
                    leading: new Icon(Icons.map),
                    onTap: () {
                      print(c.categoria);
                      Vuex.goToPage(
                          destino: AppRoutes.DETALLECATEGORIA, args: c);
                    },
                  );
                },
                itemCount: _.arrCategorias.length),
          );
        }
      },
    );
  }
}
