import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/agenda/agenda_controller.dart';

class AgendaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AgendaController());
  }
}
