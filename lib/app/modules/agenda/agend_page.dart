import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/agenda/agenda_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
//import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class AgendaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AgendaController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Agendar citas"),
                  SizedBox(height: 150),
                  Text("Horrio 1"),
                  Text("Horrio 2"),
                  Text("Horrio 3"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        Vuex.goToPage(destino: AppRoutes.AGENTESDISPONIBLES);
                        //Get.offNamed(AppRoutes.AGENTESDISPONIBLES);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Ver agentes disponibles...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
