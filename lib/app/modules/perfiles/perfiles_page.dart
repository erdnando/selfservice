import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/perfiles/perfiles_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class PerfilesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PerfilesController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Perfiles"),
                  SizedBox(height: 150),
                  Text("Costo 34,000"),
                  Text("Subtotal 34,000"),
                  Text("Total 34,500"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        //Get.offNamed(AppRoutes.HOME);
                        Vuex.goToPage(destino: AppRoutes.COMPROBANTE);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("A comprobantes...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
