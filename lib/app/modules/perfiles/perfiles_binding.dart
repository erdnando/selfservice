import 'package:get/get.dart';
import 'perfiles_controller.dart';

class PerfilesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PerfilesController());
  }
}
