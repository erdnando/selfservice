import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/loading/loading_controller.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoadingController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 30),
                  Text("Loading............................"),
                  SizedBox(height: 150),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
