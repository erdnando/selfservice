import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/registro/registro_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class RegistroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegistroController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Iniciar registro"),
                  SizedBox(height: 150),
                  Text("Su numero: +52 5612072138"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        // Get.offNamed(AppRoutes.VERIFICACION);
                        Vuex.goToPage(destino: AppRoutes.VERIFICACION);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("A la verificación...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
