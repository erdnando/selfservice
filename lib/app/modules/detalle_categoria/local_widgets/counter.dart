import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../detalle_categoria_controller.dart';

class Contador extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetalleCategoriaController>(
      id: "idCounter",
      builder: (_) => Text(_.counter.toString()),
    );
  }
}
