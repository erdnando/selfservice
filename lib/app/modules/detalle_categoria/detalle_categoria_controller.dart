import 'dart:async';

import 'package:faker/faker.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:getx_pattern/app/data/models/categoria.dart';

class DetalleCategoriaController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();
  RxInt counter = 0.obs;

  Categorias _categoriaSeleccionada = Get.arguments as Categorias;
  Categorias get categoria => _categoriaSeleccionada;

  RxString alias = "".obs;

  RxString message = "".obs;

  RxString searchText = "".obs;

  late Timer _timer;
  final _faker = Faker();

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
    _timer.cancel();
  }

  _init() async {
    /*ever(message, (_) {
      print("mensaje ha cambiado ${message.value}");
    });*/

    //realiza llamadas retardadas a eventos
    debounce(searchText, (_) {
      print("llamando api de busqueda..${searchText.value}");
    }, time: Duration(seconds: 1));

    try {
      print("on controller detalle categorias....");

      print("Argumento recibido ${Get.arguments}");
      faker();
    } catch (e) {
      print(e);
    }
  }

  void txtChanged(String value) {
    this.searchText.value = value;
    //print(value);
    //update(['aliasCategoria']);
  }

  void contador() {
    this.counter.value++;
    //update(['idCounter']); //must call this function to update the view!!!!
    //and you can define which controls to be update inside the []
  }

  void faker() {
    _timer = Timer.periodic(Duration(seconds: 5), (timer) {
      message.value = _faker.person.firstName();
    });
  }
}
