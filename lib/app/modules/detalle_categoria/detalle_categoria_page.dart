import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/detalle_categoria/detalle_categoria_controller.dart';
import 'package:getx_pattern/app/modules/detalle_categoria/local_widgets/counter.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class DetalleCategoriaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetalleCategoriaController>(
      builder: (_) {
        print("en builder de sub categorias");
        return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Vuex.goToPage(destino: AppRoutes.HOME);
                      print('go home');
                    },
                  );
                },
              ),
              title: Text('Sub categorias'),
              actions: <Widget>[
                IconButton(
                  icon: new Icon(Icons.merge_type),
                  onPressed: () => print('hi on icon action'),
                ),
              ],
            ),
            body: GestureDetector(
              //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Container(
                  color: Colors.transparent,
                  width: double.infinity,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Detalle categoria"),
                        SizedBox(height: 150),
                        Text("Categoria: ${_.categoria.categoria}"),
                        Text("Id: ${_.categoria.categoriaId}"),
                        Text("Created at: ${_.categoria.createdAt}"),
                        /*GetBuilder<DetalleCategoriaController>(
                      id: "aliasCategoria",
                      builder: (_) {
                        return Text(_.txtSubcategoriaAlias);
                      },
                    ),*/
//                  Obx(() => Text(_.alias.value.toString())),
                        Obx(() => Text(_.message.value.toString())),
                        Obx(() => Text(_.searchText.value.toString())),
                        TextField(onChanged: _.txtChanged),
                        //Contador(),
                        Obx(() => Text(_.counter.value.toString())),
                        SizedBox(height: 150),
                        TextButton(
                            onPressed: () {
                              Vuex.goToPage(destino: AppRoutes.AGENDA);
                            },
                            style: TextButton.styleFrom(
                                backgroundColor: Colors.teal,
                                primary: Colors.white,
                                onSurface: Colors.grey),
                            child: Text("A la agenda...")),
                        FloatingActionButton(
                            child: Icon(Icons.add),
                            onPressed: () {
                              _.contador();
                            }),
                        SizedBox(height: 50),
                      ])),
            ));
      },
    );
  }
}
