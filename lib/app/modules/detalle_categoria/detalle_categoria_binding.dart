import 'package:get/get.dart';
import 'detalle_categoria_controller.dart';

class DetalleCategoriaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetalleCategoriaController());
  }
}
