import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/pagar/pagar_controller.dart';

class PagarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PagarController());
  }
}
