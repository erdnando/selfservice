import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/comprobante/comprobante_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
//import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class ComprobantePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ComprobanteController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Comprobante"),
                  SizedBox(height: 150),
                  Text("Costo 33,000"),
                  Text("Subtotal 34,000"),
                  Text("Total 34,500"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        // Get.offNamed(AppRoutes.PAGAR);
                        Vuex.goToPage(destino: AppRoutes.PAGAR);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text(" A Pagar...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
