import 'package:get/get.dart';
import 'comprobante_controller.dart';

class ComprobanteBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ComprobanteController());
  }
}
