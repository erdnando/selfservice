import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/splash/splash_controller.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 30),
                  Text("Spalsh..."),
                  SizedBox(height: 150),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
