import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:getx_pattern/app/data/models/token.dart';
import 'package:getx_pattern/app/data/repositories/local/local_authentication_repository.dart';
import 'package:getx_pattern/app/data/repositories/remote/selfservice_repository.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class SplashController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //DB local
  final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();
  //repository to interact with selfservice api
  final SelfServiceRepository _baseRepository =
      Get.find<SelfServiceRepository>();

  late Token _myToken;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      //obt to control the back history in android
      BackButtonInterceptor.add(myInterceptor);

      print("on controller splash...");
      // RequestToken requestToken = await _repository.newRequestToken();
      // Store.TOKEN_GENERADO = requestToken.requestToken;

      //como un setTimeOut
      // await Future.delayed(Duration(seconds: 4));

      //accede a las propiedade de una instancia inyectada
      //final RequestToken requestToken = await _repository.session;
      final bool isRegistered = await _repository.registrado;

      try {
        _myToken = await _baseRepository.newToken(
            identifier: Vuex.identifier, password: Vuex.password);

        print("Token generado");
        print(_myToken.jwt);

        Vuex.myToken = _myToken.jwt;
        print("No esta registrado");
        //redirije a una pagina
        Vuex.goToPage(
            destino: isRegistered ? AppRoutes.HOME : AppRoutes.AYUDAINICIAL);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.response) {
          Vuex.dialogo(
              titulo: "ERROR",
              mensaje: "Problemas para generar su token: " + ex.message,
              accion: Vuex.sinAcceso());
        }
      }
    } catch (e) {
      print(e);
      Vuex.dialogo(
          titulo: "ERROR",
          mensaje: "Problemas para ingresar: " + e.toString(),
          accion: Vuex.sinAcceso());
    }
  }

  //Helper to avoid use back button in smartphone
  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    print("BACK BUTTON!");
    return true;
  }
}
