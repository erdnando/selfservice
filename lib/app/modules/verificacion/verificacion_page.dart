import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/verificacion/verificacion_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class VerificacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerificacionController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Verificación del telefono"),
                  SizedBox(height: 150),
                  Text("Su código: 2 4 3 0 0 6"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        Vuex.goToPage(destino: AppRoutes.HOME);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Al home ;)...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
