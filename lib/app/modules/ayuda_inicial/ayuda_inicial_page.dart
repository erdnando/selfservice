import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/ayuda_inicial/ayuda_inicial_controller.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/vuex.dart';

class AyudaInicialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AyudaInicialController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("AyudaInicial...1"),
                  Text("Token generado:....."),
                  Text(Vuex.myToken),
                  Text(_.counter.toString()),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        Vuex.goToPage(destino: AppRoutes.REGISTRO);
                        //Get.offNamed(AppRoutes.REGISTRO);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Al registro...")),
                  FloatingActionButton(
                      child: Icon(Icons.add),
                      onPressed: () {
                        _.contador();
                      }),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
