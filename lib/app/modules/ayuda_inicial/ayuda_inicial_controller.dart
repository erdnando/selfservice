import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
//import 'package:getx_pattern/app/data/models/request_token.dart';
//import 'package:getx_pattern/app/data/repositories/local/local_authentication_repository.dart';
//import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';
//import 'package:getx_pattern/app/routes/app_routes.dart';
//import 'package:getx_pattern/app/utils/vuex.dart';

class AyudaInicialController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();
  int counter = 0;

  @override
  void onInit() {
    //to do things before render the view, for example background tasks in db or api
    super.onInit();
  }

  @override
  void onReady() {
    //to do tinghs after render the view
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller Ayuda inicial...");
      // RequestToken requestToken = await _repository.newRequestToken();
      // Store.TOKEN_GENERADO = requestToken.requestToken;

      //await Future.delayed(Duration(seconds: 5));//como un setTimeOut
      //final RequestToken requestToken = await _repository.session;

      // print('Token generado');
      // print(Store.TOKEN_GENERADO);
      //redirije a una pagina
      //  Get.offNamed(requestToken!= null ? AppRoutes.HOME : AppRoutes.LOGIN);
    } catch (e) {
      print(e);
    }
  }

  void contador() {
    this.counter++;
    update(); //must call this function to update the view
  }
}
