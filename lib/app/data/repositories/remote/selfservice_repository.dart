import 'package:get/utils.dart';
import 'package:getx_pattern/app/data/models/categoria.dart';
import 'package:getx_pattern/app/data/models/token.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/providers/remote/selfservice_api.dart';
//import 'package:meta/meta.dart' show required;

class SelfServiceRepository {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final SelfServiceApi _api = Get.find<SelfServiceApi>();

  Future<Token> newToken(
      {required String identifier, required String password}) {
    return _api.newToken(identifier: identifier, password: password);
  }

  Future<List<Categorias>> categorias() {
    return _api.categorias();
  }
}
