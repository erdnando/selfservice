class Categorias {
  Categorias({
    required this.id,
    required this.estatus,
    required this.categoria,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.categoriaId,
  });

  final String id;
  final int estatus;
  final String categoria;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String categoriaId;

  factory Categorias.fromJson(Map<String, dynamic> json) => Categorias(
        id: json["_id"],
        estatus: json["Estatus"],
        categoria: json["Categoria"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        categoriaId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Estatus": estatus,
        "Categoria": categoria,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "id": categoriaId,
      };
}
