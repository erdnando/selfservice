import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:getx_pattern/app/data/models/categoria.dart';
import 'package:getx_pattern/app/data/models/token.dart';
import 'package:getx_pattern/app/utils/diobase.dart';
import 'package:get/get.dart' as GET;
import 'package:getx_pattern/app/utils/vuex.dart';

class SelfServiceApi {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final Dio _dio = GET.Get.find<Dio>();
  final DioBase _dio = GET.Get.find<DioBase>();

  //Permite obtener un token para utilizarlo en las posteriores llamadas al api de selfservice
  Future<Token> newToken(
      {required String identifier, required String password}) async {
    final Response response = await _dio.dioService().post('/auth/local',
        data: {"identifier": identifier, "password": password});

    return Token.fromJson(response.data);
  }

  //permite obtener el listado de categorias
  Future<List<Categorias>> categorias() async {
    String myToken = Vuex.myToken;

    final Response response = await _dio.dioService().get('/categorias',
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return (response.data as List).map((e) => Categorias.fromJson(e)).toList();
  }
}
