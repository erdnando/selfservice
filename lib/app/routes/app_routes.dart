//import 'package:get/get.dart';

class AppRoutes {
  static const SPLASH = "splash";
  static const AYUDAINICIAL = "ayuda-inicial";
  static const REGISTRO = "registro";
  static const VERIFICACION = "verificacion";
  static const HOME = "home";
  static const DETAIL = "detail";
  static const DETALLECATEGORIA = "detalle-categoria";
  static const AGENDA = "agenda";
  static const AGENTESDISPONIBLES = "agentes-disponibles";
  static const PERFILES = "perfiles";
  static const COMPROBANTE = "comprobante";
  static const PAGAR = "pagar";
  static const LOGIN = "login";
  static const LOADING = "loading";
  static const SINACCESO = "sin-acceso";
}
