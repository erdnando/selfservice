import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/agenda/agend_page.dart';
import 'package:getx_pattern/app/modules/agenda/agenda_binding.dart';
import 'package:getx_pattern/app/modules/agentes_disponibles/agentes_disponibles_binding.dart';
import 'package:getx_pattern/app/modules/agentes_disponibles/agentes_disponibles_page.dart';
import 'package:getx_pattern/app/modules/ayuda_inicial/ayuda_inicial_binding.dart';
import 'package:getx_pattern/app/modules/ayuda_inicial/ayuda_inicial_page.dart';
import 'package:getx_pattern/app/modules/comprobante/comprobante_binding.dart';
import 'package:getx_pattern/app/modules/comprobante/comprobante_page.dart';
import 'package:getx_pattern/app/modules/detail/detail_binding.dart';
import 'package:getx_pattern/app/modules/detail/detail_page.dart';
import 'package:getx_pattern/app/modules/detalle_categoria/detalle_categoria_binding.dart';
import 'package:getx_pattern/app/modules/detalle_categoria/detalle_categoria_page.dart';
import 'package:getx_pattern/app/modules/home/home_binding.dart';
import 'package:getx_pattern/app/modules/home/home_page.dart';
import 'package:getx_pattern/app/modules/loading/loading_binding.dart';
import 'package:getx_pattern/app/modules/loading/loading_page.dart';
import 'package:getx_pattern/app/modules/login/login_binding.dart';
import 'package:getx_pattern/app/modules/login/login_page.dart';
import 'package:getx_pattern/app/modules/pagar/pagar_binding.dart';
import 'package:getx_pattern/app/modules/pagar/pagar_page.dart';
import 'package:getx_pattern/app/modules/perfiles/perfiles_binding.dart';
import 'package:getx_pattern/app/modules/perfiles/perfiles_page.dart';
import 'package:getx_pattern/app/modules/registro/registro_binding.dart';
import 'package:getx_pattern/app/modules/registro/registro_page.dart';
import 'package:getx_pattern/app/modules/sin_acceso/sin_acceso_binding.dart';
import 'package:getx_pattern/app/modules/sin_acceso/sin_acceso_page.dart';
import 'package:getx_pattern/app/modules/splash/splash_binding.dart';
import 'package:getx_pattern/app/modules/splash/splash_page.dart';
import 'package:getx_pattern/app/modules/verificacion/verificacion_binding.dart';
import 'package:getx_pattern/app/modules/verificacion/verificacion_page.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.SPLASH,
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.AYUDAINICIAL,
        page: () => AyudaInicialPage(),
        binding: AyudaInicialBinding()),
    GetPage(
        name: AppRoutes.REGISTRO,
        page: () => RegistroPage(),
        binding: RegistroBinding()),
    GetPage(
        name: AppRoutes.VERIFICACION,
        page: () => VerificacionPage(),
        binding: VerificacionBinding()),
    GetPage(
        name: AppRoutes.HOME, page: () => HomePage(), binding: HomeBinding()),
    GetPage(
        name: AppRoutes.DETAIL,
        page: () => DetailPage(),
        binding: DetailBinding()),
    GetPage(
        name: AppRoutes.DETALLECATEGORIA,
        page: () => DetalleCategoriaPage(),
        binding: DetalleCategoriaBinding()),
    GetPage(
        name: AppRoutes.AGENDA,
        page: () => AgendaPage(),
        binding: AgendaBinding()),
    GetPage(
        name: AppRoutes.AGENTESDISPONIBLES,
        page: () => AgentesDisponiblesPage(),
        binding: AgentesDisponiblesBinding()),
    GetPage(
        name: AppRoutes.PERFILES,
        page: () => PerfilesPage(),
        binding: PerfilesBinding()),
    GetPage(
        name: AppRoutes.COMPROBANTE,
        page: () => ComprobantePage(),
        binding: ComprobanteBinding()),
    GetPage(
        name: AppRoutes.PAGAR,
        page: () => PagarPage(),
        binding: PagarBinding()),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => LoginPage(),
        binding: LoginBinding()),
    GetPage(
        name: AppRoutes.LOADING,
        page: () => LoadingPage(),
        binding: LoadingBinding()),
    GetPage(
        name: AppRoutes.SINACCESO,
        page: () => SinAccesoPage(),
        binding: SinAccesoBinding()),
  ];
}
